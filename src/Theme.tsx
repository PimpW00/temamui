import { createMuiTheme } from "@material-ui/core/styles";

export default createMuiTheme({
  typography: {
    fontFamily: ["Circular"].join(","),
    fontSize: 16,
    h1: {
      fontFamily: "Circular Bold",
      fontSize: "2.5rem",
      fontWeight: "bold",
      lineHeight: "120%",
    },
    h2: {
      fontFamily: "Circular Bold",
      fontSize: "2rem",
      fontWeight: "bold",
      lineHeight: "125%",
    },
    h3: {
      fontFamily: "Circular Bold",
      fontSize: "1.5rem",
      fontWeight: "bold",
      lineHeight: "120%",
    },
    h4: {
      fontFamily: "Circular Bold",
      fontSize: "1.25rem",
      fontWeight: "bold",
      lineHeight: "120%",
    },
    h5: {
      fontFamily: "Circular Bold",
      fontSize: "1rem",
      fontWeight: "bold",
      lineHeight: "150%",
    },
    h6: {
      fontFamily: "Circular Bold",
      fontSize: ".875rem",
      fontWeight: "bold",
      lineHeight: "114%",
    },
    button: {
      fontSize: "12px",
      height: "48px",
      fontFamily: ["Circular"].join(","),
    },
  },
  palette: {
    nsBlue: {
      main: "#106FF3",
      dark: "#0052C4",
      light: "#579BFB",
    },
    nsLightBlue: {
      main: "#E7F1FF",
      light: "#c9d1dd",
    },
    nsDis: {
      main: "#4B8CBD",
      dark: "#EBF3F2",
      light: "#F5F7F9",
    },

    nsGrey: {
      light: "#DDDDDD",
      main: "#767676",
      dark: "#545454",
    },

    nsRed: {
      main: "#DD3636",
      light: "#ee9faf",
      dark: "#C32113",
    },

    nsBlack: {
      light: "#333333",
      main: "#1D1D1D",
    },
  },
});
