import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  letter: {
    [theme.breakpoints.up("xs")]: {
      lineHeight: "133%",
      fontSize: ".75rem",
    },
    [theme.breakpoints.up("sm")]: {
      lineHeight: "171%",
      fontSize: ".875rem",
    },
    [theme.breakpoints.up("md")]: {
      lineHeight: "150%",
      fontSize: "1rem",
    },
    [theme.breakpoints.up("lg")]: {
      lineHeight: "178%",
      fontSize: "1.125rem",
    },
  },
}));

const HeaderMain = () => {
  const classes = useStyles();
  return (
    <div>
      <Typography variant="h1">Unlock the writen Word</Typography>
      <Typography variant="h2">Unlock the writen Word</Typography>
      <Typography variant="h3">Unlock the writen Word</Typography>
      <Typography variant="h4">Unlock the writen Word</Typography>
      <Typography variant="h5">Unlock the writen Word</Typography>
      <Typography variant="h6">Unlock the writen Word</Typography>
      <p className={classes.letter}>tamaño tipografia</p>
    </div>
  );
};

export default HeaderMain;
