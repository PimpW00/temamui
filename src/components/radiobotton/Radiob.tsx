import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Radio, { RadioProps } from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Theme from "../../Theme";

const useStyles = makeStyles({
  containerRB: {
    display: "flex",
  },
  box: {
    backgroundColor: "transparent",
    borderRadius: "5px",
    width: "200px",
    padding: "10px",
    margin: "20px",
    "&:focus-within": {
      border: "2px solid #0b44cc",
      borderRadius: "5px",
      width: "220px",
      padding: "10px",
      margin: "20px",
    },
  },
  box2: {
    backgroundColor: "#f5f5f6",
    borderRadius: "5px",
    width: "300px",
    padding: "10px",
    margin: "20px",
    "&:focus-within": {
      border: "2px solid #0b44cc",
      borderRadius: "5px",
      width: "300px",
      padding: "10px",
      margin: "20px",
    },
  },
  root: {
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
  icon: {
    borderRadius: "50%",
    width: 16,
    height: 16,
    boxShadow:
      "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
    backgroundColor: "#f5f8fa",
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
    "input:hover ~ &": {
      backgroundColor: "#ebf1f5",
    },

    "input:disabled ~ &": {
      boxShadow: "none",
      background: "rgba(206,217,224,.5)",
    },
  },
  checkedIcon: {
    backgroundColor: Theme.palette.nsBlack.main,
    backgroundImage:
      "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
    "&:before": {
      display: "block",
      width: 15,
      height: 15,
      backgroundImage: "radial-gradient(#fff,#fff 28%,transparent 32%)",
      content: '""',
    },
    "input:hover ~ &": {
      backgroundColor: Theme.palette.nsBlack.main,
    },
  },
});

function StyledRadio(props: RadioProps) {
  const classes = useStyles();

  return (
    <Radio
      className={classes.root}
      disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}

export default function CustomizedRadios(props: any) {
  const classes = useStyles(props);
  return (
    <RadioGroup>
      <div className={classes.containerRB}>
        <div className={classes.box}>
          <FormControlLabel
            value="btn1"
            control={<StyledRadio />}
            label="Type anything here"
          />
        </div>
        <div className={classes.box2}>
          <FormControlLabel
            value="btn2"
            control={<StyledRadio />}
            label="Type anything here"
          />
        </div>
      </div>
    </RadioGroup>
  );
}
