import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
  primary: {
    background: theme.palette.nsBlue.main,
    color: "white",
    borderRadius: "7px",
    width: "200px",
    padding: "0 30px",
    "&:hover": {
      background: theme.palette.nsBlue.dark,
    },
    "&:focus": {
      background: theme.palette.nsBlue.dark,
      textColor: "white",
      border: "2px solid",
      borderColor: theme.palette.nsBlue.dark,
      boxShadow: "0px 0px 0px 2px #FFF inset",
      borderRadius: "7px",
      opacity: ".8",
    },
    "&:disabled": {
      background: theme.palette.nsDis.main,
      color: "white",
    },
  },
  secondary: {
    background: theme.palette.nsLightBlue.main,
    borderRadius: "3px",
    color: theme.palette.nsBlue.main,
    height: "48px",
    width: "200px",
    padding: "0 30px",
    "&:hover": {
      background: theme.palette.nsLightBlue.light,
      color: theme.palette.nsBlue.dark,
    },
    "&:focus": {
      border: "2px solid",
      borderColor: theme.palette.nsBlue.dark,
      boxShadow: "0px 0px 0px 2px #FFF inset",
      borderRadius: "7px",
    },

    "&:disabled": {
      background: theme.palette.nsDis.dark,
      color: theme.palette.nsDis.main,
    },
  },
  tertiary: {
    background: "none",
    borderRadius: "3px",
    color: theme.palette.nsBlue.main,

    width: "200px",
    padding: "0 30px",
    "&:hover": {
      background: theme.palette.nsGrey.light,
      color: theme.palette.nsBlue.dark,
    },
    "&:focus": {
      border: "2px solid",
      background: theme.palette.nsGrey.light,
      borderColor: theme.palette.nsBlue.dark,
      boxShadow: "0px 0px 0px 2px #FFF inset",
      borderRadius: "7px",
    },

    "&:disabled": {
      background: "none",
      color: theme.palette.nsDis.main,
    },
  },
  danger: {
    background: theme.palette.nsRed.main,
    borderRadius: "3px",
    color: "white",
    width: "200px",
    padding: "0 30px",
    "&:hover": {
      background: theme.palette.nsRed.dark,
      color: "white",
    },
    "&:focus": {
      background: theme.palette.nsRed.dark,
      border: "2px solid",
      borderColor: theme.palette.nsBlue.dark,
      boxShadow: "0px 0px 0px 2px #FFF inset",
      borderRadius: "7px",
    },

    "&:disabled": {
      background: theme.palette.nsRed.light,
      color: "white",
    },
  },
}));

const Buttons = (props: any) => {
  const { type: string } = props;
  const classes = useStyles(props);

  function changeClass() {
    switch (props.type) {
      case "primary":
        return classes.primary;
      case "secondary":
        return classes.secondary;
      case "tertiary":
        return classes.tertiary;
      case "danger":
        return classes.danger;
      default:
        return classes.primary;
    }
  }

  return (
    <Button disableRipple className={changeClass()}>
      Type anything Here
    </Button>
  );
};

export default Buttons;
