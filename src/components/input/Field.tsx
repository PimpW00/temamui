import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import Autocomplete from "@material-ui/lab/Autocomplete";

const useStyles = makeStyles((theme) => ({
  h6: {
    position: "relative",
    top: "-10px",
    left: "-100px",
  },
  inContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    margin: "2rem",
  },

  default: {
    color: theme.palette.nsGrey.main,
    "&:hover": {
      color: theme.palette.nsRed.main,
      utlineColor: theme.palette.nsRed.main,
    },
    "&:focus": {
      border: "2px solid",
      borderColor: theme.palette.nsRed.main,
    },
  },

  in2: {
    color: theme.palette.nsGrey.main,
    "&:hover": {
      color: theme.palette.nsRed.main,
    },
    "&:focus": {
      border: "2px solid",
      borderColor: theme.palette.nsRed.dark,
    },
  },
}));

const FieldNs = (props: any) => {
  const classes = useStyles(props);
  return (
    <div className={classes.inContainer}>
      <Typography variant="h6" className={classes.h6}>
        Email address
      </Typography>
      <TextField
        className={classes.default}
        id="input"
        label="e.g. username@site.com"
        variant="outlined"
      />
      <p />
      <Typography variant="h6" className={classes.h6}>
        Email address
      </Typography>
      <Autocomplete
        id="superteacher.newsela.com"
        options={emailMuestra}
        getOptionLabel={(option) => option.title}
        style={{ width: 300 }}
        renderInput={(params) => (
          <TextField {...params} label="Combo box" variant="outlined" />
        )}
      />
      <p />
      <Typography variant="h6" className={classes.h6}>
        Email address
      </Typography>
      <TextField
        className={classes.in2}
        error
        id="outlined-error-helper-text"
        label="superteacher.newsela.com"
        helperText="please enter a valid email address"
        variant="outlined"
      />
      <p />
      <Autocomplete
        options={codeState}
        getOptionLabel={(option: any) => option.title}
        style={{ width: 100 }}
        renderInput={(params) => (
          <TextField {...params} label="States" variant="outlined" />
        )}
      />
    </div>
  );
};

// states
const codeState = [
  { title: "AL" },
  { title: "AK" },
  { title: "AZ" },
  { title: "CA" },
  { title: "CO" },
  { title: "CT" },
  { title: "DE" },
  { title: "DC" },
  { title: "FL" },
  { title: "GA" },
  { title: "HI" },
  { title: "ID" },
  { title: "IL" },
  { title: "IN" },
  { title: "IA" },
  { title: "KS" },
  { title: "KY" },
  { title: "LA" },
  { title: "ME" },
  { title: "MD" },
  { title: "MA" },
  { title: "MI" },
  { title: "MN" },
  { title: "MS" },
  { title: "MO" },
  { title: "MT" },
  { title: "NV" },
  { title: "NE" },
  { title: "NH" },
  { title: "NJ" },
  { title: "NM" },
  { title: "NY" },
  { title: "NC" },
  { title: "ND" },
  { title: "OH" },
  { title: "OK" },
  { title: "OR" },
  { title: "PA" },
  { title: "RI" },
  { title: "SC" },
  { title: "SD" },
  { title: "TN" },
  { title: "TX" },
  { title: "UT" },
  { title: "VT" },
  { title: "VA" },
  { title: "WA" },
  { title: "WV" },
  { title: "WI" },
  { title: "WY" },
];
// mail
const emailMuestra = [
  { title: "exemp@hotmail.com" },
  { title: "alex@gmail.com" },
  { title: "rod@gmail.com" },
];

export default FieldNs;
