import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox, { CheckboxProps } from "@material-ui/core/Checkbox";
import { Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    color: theme.palette.nsBlack.main,
  },
  GroupContainer: {
    display: "flex",
    flexDirection: "row",
  },
  box1: {
    backgroundColor: "transparent",
    color: theme.palette.nsBlack.main,
    borderRadius: "5px",
    width: "220px",
    padding: "10px",
    margin: "20px",

    "&:hover": {
      color: theme.palette.nsBlack.main,
      backgroundColor: "transparent",
    },
    "&:focus-within": {
      border: "2px solid #0b44cc",
      borderRadius: "5px",
      width: "220px",
      padding: "10px",
      margin: "20px",
    },
  },
  box2: {
    backgroundColor: "#f5f5f6",
    color: theme.palette.nsBlack.main,
    borderRadius: "5px",
    width: "300px",
    padding: "10px",
    margin: "20px",

    "&:hover": {
      backgroundColor: "#f5f5f6",
    },
    "&:focus-within": {
      border: "2px solid #0b44cc",
      backgroundColor: "#f5f5f6",
      boxShadow: "0px 0px 0px 2px #fff inset",
      borderRadius: "5px",
      width: "300px",
      padding: "10px",
      margin: "20px",
      color: theme.palette.nsBlack.main,
    },
  },
}));

export default function CheckboxLabel(props: CheckboxProps) {
  const classes = useStyles(props);
  const [state, setState] = React.useState({
    checkedA: false,
    checkedB: false,
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  return (
    <FormGroup>
      <div className={classes.GroupContainer}>
        <div className={classes.box1}>
          <FormControlLabel
            control={
              <Checkbox
                className={classes.root}
                color="default"
                checked={state.checkedA}
                onChange={handleChange}
                name="checkedA"
              />
            }
            label="Type anything here"
          />
        </div>
        <div className={classes.box2}>
          <FormControlLabel
            control={
              <Checkbox
                className={classes.root}
                color="default"
                checked={state.checkedB}
                onChange={handleChange}
                name="checkedB"
              />
            }
            label="Type anything here"
          />
        </div>
      </div>
    </FormGroup>
  );
}
