import * as createPalette from "@material-ui/core/styles/createPalette";
declare module "@material-ui/core/styles/createPalette";
declare module "@material-ui/core/styles/createPalette" {
  interface PaletteOptions {
    nsBlue?: PaletteColorOptions;
    nsLightBlue?: PaletteColorOptions;
    nsPurple?: PaletteColorOptions;
    nsBlack?: PaletteColorOptions;
    nsDis?: PaletteColorOptions;
    nsGrey?: PaletteColorOptions;
    nsRed?: PaletteColorOptions;
  }
  interface Palette {
    nsBlue: PaletteColor;
    nsLightBlue: PaletteColor;
    nsPurple: PaletteColor;
    nsDis: PaletteColor;
    nsBlack: PaletteColor;
    nsGrey: PaletteColor;
    nsRed: PaletteColor;
  }
}
