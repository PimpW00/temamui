import "./App.css";
import CheckboxLabel from "./components/Checkbox/CheckBox";
import HeaderMain from "./components/header/Header";
import Buttons from "./components/button/Button";
import FieldNs from "./components/input/Field";
import Radiob from "./components/radiobotton/Radiob";

function App() {
  return (
    <div className="App">
      <HeaderMain />
      <Buttons type="danger" />
      <FieldNs />
      <Radiob />
      <CheckboxLabel />
    </div>
  );
}

export default App;
